FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y curl gnupg2 && \
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs && \
    apt-get install -y default-jdk && \
    apt-get install -y ruby-full && \
    apt-get install -y maven

# Устанавливаем версию Maven через ссылку на Apache Maven
# RUN wget -q -O - https://www.apache.org/dist/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz | tar -xzf - -C /opt
# ENV MAVEN_HOME /opt/apache-maven-3.8.1
# ENV PATH $PATH:$MAVEN_HOME/bin

# Поддерживаем рабочую директорию
WORKDIR /app

# Выводим информацию о версиях установленных пакетов
RUN node -v && npm -v && java -version && ruby -v && mvn -v

# Команда по умолчанию при запуске контейнера
CMD ["bash"]